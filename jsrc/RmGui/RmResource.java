package RmGui;

class RmResource {
    private final String name;
    private final Integer maxTot;
    private final Integer grantedTot;
    private final Integer maxPP;
    private final Integer grantedPP;
    private final Integer handle;
    private final String app;
    private final String computer;
    private final String client;
    private final Integer pid;
    private final String partition;

    public RmResource(final String name,
                      final Integer maxTot,
                      final Integer grantedTot,
                      final Integer maxPP,
                      final Integer grantedPP,
                      final Integer handle,
                      final String app,
                      final String computer,
                      final String client,
                      final Integer pid,
                      final String partition)
    {
        super();

        this.name = name;
        this.maxTot = maxTot;
        this.grantedTot = grantedTot;
        this.maxPP = maxPP;
        this.grantedPP = grantedPP;
        this.handle = handle;
        this.app = app;
        this.computer = computer;
        this.client = client;
        this.pid = pid;
        this.partition = partition;
    }

    public String getName() {
        return this.name;
    }

    public Integer getMaxTot() {
        return this.maxTot;
    }

    public Integer getGrantedTot() {
        return this.grantedTot;
    }

    public Integer getMaxPP() {
        return this.maxPP;
    }

    public Integer getGrantedPP() {
        return this.grantedPP;
    }

    public Integer getHandle() {
        return this.handle;
    }

    public String getApp() {
        return this.app;
    }

    public String getComputer() {
        return this.computer;
    }

    public String getClient() {
        return this.client;
    }

    public Integer getPid() {
        return this.pid;
    }

    public String getPartition() {
        return this.partition;
    }
}
