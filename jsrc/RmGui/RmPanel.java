package RmGui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.HighlighterFactory;
import org.jdesktop.swingx.error.ErrorInfo;

import am.client.RequestorInfo;
import am.client.ServerInterrogator;
import am.xacml.context.impl.RMResource;
import daq.rmgr.AllocatedResource;
import daq.rmgr.RMClientImpl;


public class RmPanel {
    private final static String userName = System.getProperty("user.name", "unknown");

    private final JFrame mainFrame = new JFrame();
    private final JPopupMenu popupMenu = new JPopupMenu();
    private final DefaultListModel<String> partListModel = new DefaultListModel<>();
    private final JList<String> partList = new JList<String>(this.partListModel);
    private final RmTableModel tableModel = new RmTableModel();
    private final JXTable resTable = new JXTable(this.tableModel);
    private final JProgressBar progressbar = new JProgressBar();

    // The following thread local members are used for objects that may not be thread-safe
    private final static ThreadLocal<ServerInterrogator> amServerInterrogator = new ThreadLocal<ServerInterrogator>() {
        @Override
        protected ServerInterrogator initialValue() {
            return new ServerInterrogator();
        }
    };

    private final static ThreadLocal<RMClientImpl> rmClient = new ThreadLocal<RMClientImpl>() {
        @Override
        protected RMClientImpl initialValue() {
            return new RMClientImpl();
        }
    };

    private final static ThreadLocal<RequestorInfo> amRequestor = new ThreadLocal<RequestorInfo>() {
        @Override
        protected RequestorInfo initialValue() {
            final StringBuilder hostName = new StringBuilder();
            try {
                hostName.append(InetAddress.getLocalHost().getCanonicalHostName());
            }
            catch(final UnknownHostException ex) {
                hostName.append("unknown");
            }

            return new RequestorInfo(RmPanel.userName, hostName.toString());
        }
    };

    // SwingWorker used to load the list of available partitions
    private class PartitionsLoader extends SwingWorker<Set<String>, Void> {
        private PartitionsLoader() {
        }

        @Override
        protected Set<String> doInBackground() throws Exception {
            RmPanel.this.setBusy(true);

            final Set<String> parts = new TreeSet<>();
            parts.add("initial"); // The initial partition is never in the list

            final ipc.PartitionEnumeration partitions = new ipc.PartitionEnumeration();
            while(partitions.hasMoreElements() == true) {
                parts.add(partitions.nextElement().getName());
            }

            return parts;
        }

        @Override
        protected void done() {
            RmPanel.this.partListModel.clear();

            try {
                final Set<String> parts = this.get();
                for(final String p : parts) {
                    RmPanel.this.partListModel.add(RmPanel.this.partListModel.size(), p);
                }
            }
            catch(final InterruptedException ex) {
                // Restore the interrupted state of the EDT
                Thread.currentThread().interrupt();
            }
            catch(final ExecutionException ex) {
                JXErrorPane.showDialog(RmPanel.this.mainFrame,
                                       new ErrorInfo("RM GUI - Error",
                                                     "Cannot retrieve the list of available partitions. Is the \"initial\" partition up?",
                                                     null,
                                                     null,
                                                     ex,
                                                     null,
                                                     null));
            }
            finally {
                RmPanel.this.setBusy(false);
                RmPanel.this.resetProgressbar();

                // After the load of the partition list, better to clean the table if nothing is selected
                final String sel = RmPanel.this.partList.getSelectedValue();
                if(sel == null) {
                    RmPanel.this.tableModel.clearTable();
                }
            }
        }
    }

    // SwingWorker used to load resources for the selected partition
    private class ResourcesLoader extends SwingWorker<AllocatedResource[], Void> {
        private final String partitionName;

        private ResourcesLoader(final String partitionName) {
            this.partitionName = partitionName;
        }

        @Override
        protected AllocatedResource[] doInBackground() throws Exception {
            // Disable the panel while resources are loaded
            RmPanel.this.setBusy(true);

            final RMClientImpl rm = RmPanel.rmClient.get();
            return rm.getPartitionInfo(this.partitionName);
        }

        @Override
        protected void done() {
            // Be sure that the partition's selection did not change in the while
            try {
                if(this.partitionName.equals(RmPanel.this.partList.getSelectedValue()) == true) {
                    final AllocatedResource[] res = this.get();
                    RmPanel.this.tableModel.addResources(res);
                }
            }
            catch(final InterruptedException ex) {
                // Restore the interrupted state of the EDT
                Thread.currentThread().interrupt();
            }
            catch(final ExecutionException ex) {
                RmPanel.this.tableModel.clearTable();

                JXErrorPane.showDialog(RmPanel.this.mainFrame,
                                       new ErrorInfo("RM GUI - Error",
                                                     "Cannot retrieve the list of resources for the selected partition: issues contacting the ResourceManager server.",
                                                     null,
                                                     null,
                                                     ex,
                                                     null,
                                                     null));
            }
            finally {
                RmPanel.this.resetProgressbar();
                RmPanel.this.setBusy(false);
            }
        }
    }

    // SwingWorker used to free resources
    private class ResourcesDeallocator extends SwingWorker<Void, Void> {
        private final AtomicInteger remainingDeallocations;
        private final RmResource rmResource;
        private final StringBuffer errMsgs;

        private ResourcesDeallocator(final AtomicInteger remainingDeallocations, final RmResource resource, final StringBuffer errs) {
            this.remainingDeallocations = remainingDeallocations;
            this.rmResource = resource;
            this.errMsgs = errs;
        }

        @Override
        protected Void doInBackground() throws Exception {
            RmPanel.this.setBusy(true);

            final RMResource amRes = new RMResource(this.rmResource.getPartition());
            amRes.setActionANY();

            final boolean allowedByAM;
            if(RmPanel.userName.equals(this.rmResource.getClient()) == false) {
                allowedByAM = RmPanel.amServerInterrogator.get().isAuthorizationGranted(amRes, RmPanel.amRequestor.get());
            } else {
                allowedByAM = true;
            }

            if(allowedByAM == true) {
                final RMClientImpl rm = RmPanel.rmClient.get();
                rm.freeResources(this.rmResource.getHandle().intValue());
            } else {
                final String msg = "<b>Failed to free resource " + this.rmResource.getName() + " with handle "
                                   + this.rmResource.getHandle() + "</b>: not allowed by the AM<br/>";
                this.errMsgs.append(msg);
            }

            return null;
        }

        @Override
        protected void done() {
            try {
                this.get();
            }
            catch(final InterruptedException ex) {
                // Restore the interrupted state of the EDT
                Thread.currentThread().interrupt();
            }
            catch(final ExecutionException ex) {
                final String msg = "<b>Failed to free resource " + this.rmResource.getName() + " with handle "
                                   + this.rmResource.getHandle() + "</b>: " + ex + "<br/>";
                this.errMsgs.append(msg);
            }
            finally {
                // The last one sets back the normal cursor and refreshes the table for the selected partition
                final int remaining = this.remainingDeallocations.decrementAndGet();
                if(remaining == 0) {
                    RmPanel.this.resetProgressbar();
                    RmPanel.this.setBusy(false);

                    // Refresh the table
                    final String sel = RmPanel.this.partList.getSelectedValue();
                    if(sel != null) {
                        RmPanel.this.setProgressbarIndeterminate("Reloading the list of granted resources");
                        new ResourcesLoader(sel).execute();
                    } else {
                        RmPanel.this.tableModel.clearTable();
                    }

                    // Show any error
                    if(this.errMsgs.length() != 0) {
                        JXErrorPane.showDialog(RmPanel.this.mainFrame,
                                               new ErrorInfo("RM GUI - Error",
                                                             "Some resources could not be freed because some errors occurred.",
                                                             this.errMsgs.toString(),
                                                             null,
                                                             null,
                                                             null,
                                                             null));
                    }
                } else {
                    RmPanel.this.incrementProgressbar();
                }
            }
        }
    }

    public RmPanel() {
    }

    void start() {
        this.createGUI();
        this.fillGUI();
    }

    private void setBusy(final boolean busy) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                RmPanel.this.resTable.setEnabled(!busy);
                RmPanel.this.partList.setEnabled(!busy);
                RmPanel.this.mainFrame.setCursor(busy == true ? Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR)
                                                             : Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        });
    }

    private void setProgressbarIndeterminate(final String message) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true);

        if(this.progressbar.isIndeterminate() == false) {
            this.progressbar.setIndeterminate(true);
        }

        if(this.progressbar.isStringPainted() == false) {
            this.progressbar.setStringPainted(true);
        }
        this.progressbar.setString(message.isEmpty() == true ? null : message);
    }

    private void setProgressbar(final int min, final int max, final String message) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true);

        if(this.progressbar.isIndeterminate() == true) {
            this.progressbar.setIndeterminate(false);
        }

        if(this.progressbar.isStringPainted() == false) {
            this.progressbar.setStringPainted(true);
        }
        this.progressbar.setString(message.isEmpty() == true ? null : message);

        this.progressbar.setMinimum(min);
        this.progressbar.setMaximum(max);
    }

    private void incrementProgressbar() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true);

        if(this.progressbar.isStringPainted() == false) {
            this.progressbar.setStringPainted(true);
        }

        int v = this.progressbar.getValue();
        this.progressbar.setValue(++v);
    }

    private void resetProgressbar() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true);

        if(this.progressbar.isIndeterminate() == true) {
            this.progressbar.setIndeterminate(false);
        }

        this.progressbar.setStringPainted(false);
        this.progressbar.setValue(0);
    }

    private void fillGUI() {
        new PartitionsLoader().execute();
    }

    private void createGUI() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true);

        // The main frame
        this.mainFrame.setLayout(new BorderLayout());
        this.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.mainFrame.setTitle("Resource Manager GUI");

        // The split pane
        final JSplitPane split = new JSplitPane();
        this.mainFrame.add(split, BorderLayout.CENTER);

        // At left the list of partitions
        final JPanel partListPanel = new JPanel(new BorderLayout());

        final JLabel partListLabel = new JLabel("Available Partitions");
        partListLabel.setOpaque(true);
        partListLabel.setBackground(Color.ORANGE);
        partListLabel.setVerticalAlignment(SwingConstants.CENTER);
        partListLabel.setHorizontalAlignment(SwingConstants.CENTER);
        partListPanel.add(partListLabel, BorderLayout.NORTH);

        this.partList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(final ListSelectionEvent e) {
                if(e.getValueIsAdjusting() == false) {
                    final String sel = RmPanel.this.partList.getSelectedValue();
                    if(sel != null) {
                        RmPanel.this.setProgressbarIndeterminate("Loading the list of granted resources for the selected partition");
                        new ResourcesLoader(sel).execute();
                    }
                }
            }
        });
        this.partList.setVisibleRowCount(-1);
        this.partList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        final JScrollPane ps = new JScrollPane(this.partList);
        ps.setPreferredSize(new Dimension(150, 600));
        partListPanel.add(ps, BorderLayout.CENTER);

        split.setLeftComponent(partListPanel);

        // At right the table with the resources
        this.resTable.setEditable(false);
        this.resTable.setSortsOnUpdates(true);
        this.resTable.setHighlighters(HighlighterFactory.createAlternateStriping());
        this.resTable.setColumnControlVisible(true);
        this.resTable.setHorizontalScrollEnabled(true);
        this.resTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(final MouseEvent e) {
                if(javax.swing.SwingUtilities.isRightMouseButton(e) == true) {
                    // If the mouse is pressed for a row that is not selected, then select it
                    final int mouseRow = RmPanel.this.resTable.rowAtPoint(new Point(e.getX(), e.getY()));
                    if((mouseRow != -1) && (RmPanel.this.resTable.isRowSelected(mouseRow) == false)) {
                        RmPanel.this.resTable.getSelectionModel().setSelectionInterval(mouseRow, mouseRow);
                    }

                    final int[] selRows = RmPanel.this.resTable.getSelectedRows();
                    if(selRows.length != 0) {
                        RmPanel.this.popupMenu.show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            }
        });

        split.setRightComponent(new JScrollPane(RmPanel.this.resTable));
        split.setDividerLocation(-1);

        // Setup the pop-up menu
        final JMenuItem freeResourceItem = new JMenuItem("Free resource(s)");
        freeResourceItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final int[] selRows = RmPanel.this.resTable.getSelectedRows();
                final AtomicInteger count = new AtomicInteger(selRows.length);
                final StringBuffer errs = new StringBuffer();
                for(final int r : selRows) {
                    RmPanel.this.setProgressbar(0, count.get(), "Freeing the selected resources");
                    new ResourcesDeallocator(count,
                                             RmPanel.this.tableModel.getResource(RmPanel.this.resTable.convertRowIndexToModel(r)),
                                             errs).execute();
                }
            }
        });

        this.popupMenu.add(freeResourceItem);

        // The progress bar
        this.mainFrame.add(this.progressbar, BorderLayout.SOUTH);

        // The tool-bar
        final JToolBar toolBar = new JToolBar();
        final JButton reloadPartButton = new JButton("Reload Partitions");
        reloadPartButton.setToolTipText("Reload the list of available partition");
        reloadPartButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                RmPanel.this.setProgressbarIndeterminate("Reloading the list of available partitions");
                new PartitionsLoader().execute();
            }
        });
        toolBar.add(reloadPartButton);

        final JButton reloadResButton = new JButton("Reload Resources");
        reloadResButton.setToolTipText("Reload the list of granted resources for the selected partition");
        reloadResButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final String sel = RmPanel.this.partList.getSelectedValue();
                if(sel != null) {
                    RmPanel.this.setProgressbarIndeterminate("Reloading the list of granted resources for the selected partition");
                    new ResourcesLoader(sel).execute();
                }
            }
        });
        toolBar.addSeparator();
        toolBar.add(reloadResButton);

        this.mainFrame.add(toolBar, BorderLayout.NORTH);

        // Set the frame's dimension and show it
        final Dimension size = new Dimension(1100, 600);
        this.mainFrame.setSize(size);
        this.mainFrame.setPreferredSize(size);
        this.mainFrame.pack();
        this.mainFrame.setVisible(true);
    }
}
