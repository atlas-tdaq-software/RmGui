package RmGui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import daq.rmgr.AllocatedResource;
import daq.rmgr.ResourceType;


class RmTableModel extends AbstractTableModel {
    private final static long serialVersionUID = 4585825503469736726L;

    private final static String[] colNames = new String[] {"Resource", "Max Total", "Granted Total", "Max per Partition",
                                                           "Granted per Partition", "Handle", "Application Name", "Computer", "Client",
                                                           "PID", "Partition"};

    private final static Class<?>[] colClasses = new Class<?>[] {String.class, Integer.class, Integer.class, Integer.class, Integer.class,
                                                                 Integer.class, String.class, String.class, String.class, Integer.class,
                                                                 String.class};

    private final List<RmResource> rmResources = new ArrayList<>();

    public RmTableModel() {
        super();
    }

    public void addResources(final AllocatedResource[] resources) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true);

        this.rmResources.clear();

        if(resources != null) {
            for(final AllocatedResource r : resources) {
                final StringBuilder resName = new StringBuilder(r.resource);
                if(r.type.equals(ResourceType.HWR) == true) {
                    resName.append('@');
                    resName.append(r.hwId.isEmpty() ? r.computer : r.hwId);
                }

                final RmResource row = new RmResource(resName.toString(),
                                                      Integer.valueOf(r.maxTotal),
                                                      Integer.valueOf(r.allocTotal),
                                                      Integer.valueOf(r.maxPP),
                                                      Integer.valueOf(r.allocPP),
                                                      Integer.valueOf(r.handle),
                                                      r.swObj,
                                                      r.computer,
                                                      r.client,
                                                      Integer.valueOf(r.pid),
                                                      r.partition);
                this.rmResources.add(row);
            }
        }

        this.fireTableDataChanged();
    }

    public RmResource getResource(final int row) {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true);

        return this.rmResources.get(row);
    }

    public void clearTable() {
        assert (javax.swing.SwingUtilities.isEventDispatchThread() == true);

        this.rmResources.clear();
        this.fireTableDataChanged();
    }

    @Override
    public Class<?> getColumnClass(final int columnIndex) {
        return RmTableModel.colClasses[columnIndex];
    }

    @Override
    public int getRowCount() {
        return this.rmResources.size();
    }

    @Override
    public int getColumnCount() {
        return RmTableModel.colNames.length;
    }

    @Override
    public String getColumnName(final int column) {
        return RmTableModel.colNames[column];
    }

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        final RmResource r = this.rmResources.get(rowIndex);

        final Object ret;
        switch(columnIndex) {
            case 0:
                ret = r.getName();
                break;
            case 1:
                ret = r.getMaxTot();
                break;
            case 2:
                ret = r.getGrantedTot();
                break;
            case 3:
                ret = r.getMaxPP();
                break;
            case 4:
                ret = r.getGrantedPP();
                break;
            case 5:
                ret = r.getHandle();
                break;
            case 6:
                ret = r.getApp();
                break;
            case 7:
                ret = r.getComputer();
                break;
            case 8:
                ret = r.getClient();
                break;
            case 9:
                ret = r.getPid();
                break;
            case 10:
                ret = r.getPartition();
                break;
            default:
                throw new IndexOutOfBoundsException("Maximum number of columns is " + RmTableModel.colClasses.length);
        }

        return ret;
    }
}
