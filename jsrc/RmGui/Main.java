package RmGui;

public class Main {

    private Main() {
        // We do not want instances of this class to be created
    }

    public static void main(final String[] args) {
        // Start the GUI in the EDT
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final RmPanel p = new RmPanel();
                p.start();
            }
        });
    }
}
